## Everyday package operations in Guix

Back at [hpr2198 :: How awesome is Guix and why will it take over the world](http://hackerpublicradio.org/eps.php?id=2198) I wrote a comment about how I use [guix](http://guixsd.org/) in everyday practice. Here's the full episode for that comment.

The most common operations I do are:

 * `guix environment --ad-hoc ncdu`, where _ncdu_ stands for something I heard about and want to try out, or something I only use once a month. It is then “installed” in the spawned sub-shell only. This is an awesome feature.  
 <ul>
   <li>If you haven’t heard about [ncdu](https://en.wikipedia.org/wiki/ncdu), look it up.</li>
   <li>Also in `~/.bash_aliases`</li>
   <li>Also in `~/.local/share/applications`
       <ul><li>Using `stow`, of course</li></ul>
 </ul>
 * `guix package -i ncdu` if it turned out to be something I like and use every day
 * `guix pull` to get the latest definitions for this user
 * `guix package -u` to upgrade my permanently installed stuff for this user
 * `guix package -d` to erase history of what I had installed before and release these references for collection
 * `guix gc` to reclaim my precious disk space
 
 * Followup episode material:
  * _What's in my [`.bash_aliases`](https://git.scuttlebot.io/%25jGFgjma3hzJfGPG5NG3neRygqy4i6WUqm1PBKsB6iEI%3D.sha256/blob/c7b2d0bce3b000f41b31dc1decd3e89822e355b7/.bash_aliases)?_
  * _Decentralized source control, for real this time, with [`git-ssb`](http://git-ssb.celehner.com/%25RPKzL382v2fAia5HuDNHD5kkFdlP7bGvXQApSXqOBwc%3D.sha256)_
  * _What's so great about [execline](http://www.skarnet.org/software/execline/)?_
  * _What's a `stow`?_
  <ul><li>_How I got rid of stow and learned to love guix to the fullest_ (Future episode. That's not where I am today.)</li>
  <li>Listen kids, [stow is not a package manager](https://gnusocial.club/conversation/71960#notice-189418) (warning: fediverse drama ahead). It's a symlink farm manager that I use for package management.</li></ul>
  * Very short episode: _ncdu, eh?_
