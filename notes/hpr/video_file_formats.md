# The evolution of video file formats

## IFF

In the beginning, there was [IFF](https://en.wikipedia.org/wiki/Interchange_File_Format), created by Electronic Arts and
Commodore, in particular used for [ILBM](https://en.wikipedia.org/wiki/ILBM) (often named `.iff`) files. Every IFF file
is in itself a valid `FORM`, `LIST` or <code>CAT&nbsp;</code> chunk.

Chunk header is Type ID + length.

Used everywhere on the Amiga, culminating in DEVS:Datatypes and MultiView.

Big-endian, 16-bit word-aligned. No relation to TIFF.

## PNG

[PNG](https://en.wikipedia.org/wiki/Portable_Network_Graphics) adds some of its own cool ideas:
 * Protection against ASCII, CRLF/LF, DOS EOF distortion
 * critical vs ancillary (`NAME` vs `nAME`)
   * If you don't know it, don't bother reading the file.
   * c.f. e.g. ext2's COMPAT/ROCOMPAT/INCOMPAT feature flag system
 * public vs private (`NAME` vs `NaME`)
   * Defined in a standard?
 * discard-if-criticals-edited vs safe-to-copy (`NAME` vs `NAMe`)
 * So a chunk named `naMe` can be ignored, is not in any standard, and your editor can save it even if you modified the
   core data of the file. 

Defined in [RFC 2083](https://tools.ietf.org/html/rfc2083).

Every derivative has its own header.

Big-endian.

## RIFF

After IFF came [RIFF](https://en.wikipedia.org/wiki/Resource_Interchange_File_Format), which is IFF but endian-backwards
and with different standard chunks, created by IBM and
Microsoft.

Has a FOURCC-tagged standard `LIST` chunk ('LIST' + size + FOURCC + chunk0 + ... + chunkN). The whole file is a `RIFF`
chunk.

Generic chunks that could be used in any format, like the mentioned `LIST`, are supposed to have upper-case names,
format-specific chunks, like the `avih` chunk in the AVI format, lower-case.

[AVI](https://en.wikipedia.org/wiki/Audio_Video_Interleave) and [WAV](https://en.wikipedia.org/wiki/WAV)
use RIFF. [WebP](https://en.wikipedia.org/wiki/WebP) uses RIFF, but breaks the lower-case conventions and doesn't use
LIST where it could.

Little-endian, 16-bit word-aligned.

## WebP

Wraps itself in a RIFF chunk, calls itself WEBP. All chunk names are upper-case, so it breaks RIFF convention
there. Other than that, there's a simple format which is a thin wrapper around a single <code>VP8&nbsp;</code> or `VP8L`
frame chunk, and an extended format that adds metadata, color profile and animation.

Defined in [Google's WebP documentation](https://developers.google.com/speed/webp/docs/riff_container).

It's RIFF, so it's little-endian, 16-bit word-aligned.

## AVI

Defined in [AVI RIFF File Reference](https://msdn.microsoft.com/en-us/library/ms779636.aspx) over at Microsoft.

One big <code>AVI&nbsp;</code> chunk inside the `RIFF` chunk.

Wikipedia says AVI doesn't support some VBR codecs, but I don't see why -- it seems to support arbitrary chunk sizes. It
also says it makes forward-looking codecs awkward, but I don't see why you wouldn't just put frames out of order and
rely on the index for ordering, or simply buffer ahead a bit to the next keyframe. Making every player support that
adaptation seems no more difficult than making them support an entirely new envelope format.

## ASF

A.k.a. `.wma`, `.wmv`, https://en.wikipedia.org/wiki/Advanced_Systems_Format

## Ogg

Originally used for, and often confused with, [Vorbis](https://en.wikipedia.org/wiki/Vorbis).

TIL: Not Nanny Ogg, Netrek Ogg!

 * Self-synchronizing (see [Ogg page](https://en.wikipedia.org/wiki/Ogg_page))
   * Makes streaming trivial
 * Time stamps and serial numbers
 * Bitstreams, not packages, but also provides packages on top of the bitstream

Defined in [RFC 3533](https://tools.ietf.org/html/rfc3533).

## OGM

An unofficial mapping of DirectShow video codecs to ogg, making it possible to use them together with
Vorbis. It
[dumped some of the AVI/DirectShow structures in the Ogg video stream](https://forum.doom9.org/showthread.php?threadid=57217) and
then relied on DirectShow handling it correctly. Not recommended by Xiph, which recommend defining how a codec fits into
Ogg, rather than reusing the codec's AVI structs.

[Ogg/OGM/Matroska flame war](https://people.xiph.org/~xiphmont/lj-pseudocut/o-response-1.html)

----

The Markdown for these notes is available from
[my git-ssb repo](http://git-ssb.celehner.com/%25jGFgjma3hzJfGPG5NG3neRygqy4i6WUqm1PBKsB6iEI%3D.sha256/blob/a713a4a4d40f13b7a85b36c02389cdf92b11c19b/notes/hpr/video_file_formats.md)
and 
[my gitlab repo](https://gitlab.com/clacke/gists/blob/master/notes/hpr/video_file_formats.md).
It was rendered with [hashify.me](http://hashify.me), which seems to mean it was rendered by [marked](https://github.com/chjj/marked).

<!-- Local Variables: -->
<!-- fill-column: 120 -->
<!-- End: -->
