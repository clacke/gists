On GNU/Linux since [Amiga Watchtower](http://www.linux-m68k.org/faq/saynotowatchtower.html), using it as my primary OS since Debian Slink, been on Ubuntu ever since it fulfilled the derailed UserLinux dream.

These days I'm running [Ubuntu-Gnome](https://www.ubuntugnome.org/) with a side dish of [Nix](https://nixos.org/nix/) and [Guix](http://guixsd.org/), but once I get around to getting Nix and nix-daemon working in GuixSD, I'll probably switch to GuixSD+Nix. With a Debian chroot for the pieces that are still missing. :-)

You can find me on social networks at:

 - pump.io: [clacke@datamost.com](https://datamost.com/clacke)
 - GNU Social/OStatus: [clacke@social.heldscal.la](https://social.heldscal.la/clacke/)
 - Patchwork/SSB: [clacke/clackers/`@iii%2fpg32...`](https://viewer.scuttlebot.io/@iii/pg320nKa62v1ohHctlhrXYPmY5BzZ1dRjypd7Cg=.ed25519)
