Hi, I'm clacke. I've long been fascinated by Tcl, and I used it sporadically about a decade to a decade-and-a-half ago. At work, I created a GUI around a repeated XSLT transformation one of my users would have to do on a somewhat recurring basis, and in uni it was used for package definitions on the Solaris machines.

I did use it more recently, but just for some Sqlite interfacing. The Tcl and Sqlite communities have some pretty close connections, and Tcl even uses Fossil as the version control system, just like Sqlite does. Fossil is developed by Sqlite people and uses a Sqlite backend.

I find it fascinating how it's basically everything LISP isn't, especially Scheme, but it accomplishes many of the same things, but backwards. They form a sort of duality, I think. Back in the day when I was following the C2 wiki, I remember a very insistant Tcl user showing LISPers how everything they thought was impossible to do in Tcl was actually possible, just not in the way they wanted it.

The history of Tcl is also mixed in with the history of Guile. There's a link called ("[Tcl war!](http://vanderburg.org/old_pages/Tcl/war/)") in the show notes for more about this.


Tcl was on its way to greatness in the 90s, and then something happened. A long-time Tcl contributor has analyzed why, and I'm going to read the post verbatim. Link is in the show notes. It's not super recent, but the observations are timeless. The blog doesn't carry any explicit licensing, but I contacted the author who gave me permission to do basically whatever I want, as long as I provide attribution.

So here it is: Where Tcl and Tk went wrong, by David N Welton at the Dedasys Journal, posted on the 30th of March, 2010.


https://journal.dedasys.com/2010/03/30/where-tcl-and-tk-went-wrong/


Episode ideas:

 * Fossil
 * Tcl, duh

Thoughts for links:
 * Examples of what other languages uses Tk for a while (Python, Perl)
 * Avoid success at all cost
 
 ----
 
 ... in which I'm reading [Where Tcl and Tk went wrong, by David N Welton](https://journal.dedasys.com/2010/03/30/where-tcl-and-tk-went-wrong/), posted on 2017-03-30 at the Dedasys Journal.
 
 Tcl is an interesting language that does many things "wrong", especially if you're coming from a LISP perspective, and especially-especially if you're coming from a Scheme perspective. Examples are all over the C2 wiki, but probably [DynamicStringsVsFunctional](http://wiki.c2.com/?DynamicStringsVsFunctional) is the epicenter.
 
It also forms an important part of modern Scheme history, as the [Tcl War](http://vanderburg.org/old_pages/Tcl/war/) led to the creation of [Guile](https://www.gnu.org/software/guile/).

TL;DL: Tcl was successful because it found its niche as a lightweight yet capable language able to both integrate and be integrated with C code, but it fell behind on Tk look-and-feel compared to GNOME and KDE and also on other mainstream development phenomena, it ossified because it was afraid to upset its installed base, it got stuck between not-slim-enough and not-featureful-enough, the syntax is too weird, and it spiraled into losing touch with the rest of the free software world, which ultimately also affected business use.

## Further notes

 * Guile (again) faces several of these same challenges.
 * Haskell tries to [avoid success at all costs](https://www.reddit.com/r/haskell/comments/39qx15/is_this_the_right_way_to_understand_haskells/), in order to not lose the freedom to improve the language.
 * [Python](https://docs.python.org/3/library/tkinter.html) and [Perl](http://www.perltk.org/) both have Tk integrations and Python's IDLE is even implemented in it. Lua had [ltk](http://tset.de/ltcltk/index.html), but it's no longer maintained. There is even a [Tcl/Tk package for R](https://www.rdocumentation.org/packages/tcltk/versions/3.4.1).
 * Ousterhout [pronounces it OH-stir-howt](http://www.huecker.com/msw/ousterhout.shtml), which may or may not be how I pronounced it. I think the guttural sound may be reserved for the Dutch "G" and have nothing to do with "H".

## Potential episodes

 * Fossil
 * Tcl
