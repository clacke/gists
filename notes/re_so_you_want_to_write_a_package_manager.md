Draft comment on https://medium.com/@sdboyer/so-you-want-to-write-a-package-manager-4ae9c17d9527

My aspect of advice to your package manager creator person is this: Please don't make something that is terrible for others to insert into some larger packaging context. Or, in the post's terms: Please have your PDM/LPM play nice with SPMs. Obviously #Nix or #Guix, but also traditional SPMs like the deb (apt) or rpm (yum/dnf) environments.

I'm working on #racket2nix and I'm currently fighting circular dependencies. It probably won't be too bad though, I think I will have a solution by next week that just makes any dependency cycle a big derivation simultaneously setting up all the #Racket packages in the cycle.

The thing Racket gets right is that it is possible at all to just get all your data down locally and then compile without global side effects. raco install works for local directories, not just remote release packages or source code repos, and raco install and raco setup are (can be made) separate.

Our #fractalide project has previously tried to take on #cargo [0], and its conflation of build manager and package manager made it a real challenge. Also cargo has no notion of a binary library, you always Compile All The Things together. One reason given[1] is that because Rust itself in such flux, you wouldn't be able to provide a stable ABI anyway. And that makes sense. But in Nix you would be able to have a known ABI! The exact compiler used would be part of the data going into the hash, and the client app would be running against a particular hash of the library.

You will notice that `nixcrates` has not been touched since 2016, and the current standard Rust solution[2] in Nix does Compile All The Things. The second solution, carnix, forgoes cargo entirely during compilation.

[0] https://github.com/nixcloud/nixcrates

[1] https://github.com/rust-lang/cargo/issues/2552

[2] https://github.com/NixOS/nixpkgs/blob/master/doc/languages-frameworks/rust.md