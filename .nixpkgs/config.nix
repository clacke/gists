{
  packageOverrides = pkgs: let
    stdenv = pkgs.stdenv;
    fetchurl = pkgs.fetchurl;
  in
  rec {
    pidgin-with-plugins = pkgs.pidgin-with-plugins.override {
      plugins = [ pkgs.pidginsipe pkgs.purple-plugin-pack ];
    };
    nss = pkgs.nss.overrideDerivation (old: rec {
      version = "3.28.3";
      name = "nss-${version}";
      src = fetchurl {
        url = "mirror://mozilla/security/nss/releases/NSS_3_28_3_RTM/src/${name}.tar.gz";
        sha256 = "1wrx2ig6yvgywjs25hzy4szgml21hwhd7ds0ghyfybhkiq7lyg6x";
      };
    });
    sqlite = pkgs.sqlite.overrideDerivation (old: rec {
      name = "sqlite-3.17.0";
      src = fetchurl {
        url = "http://sqlite.org/2017/sqlite-autoconf-3170000.tar.gz";
        sha256 = "0k472gq0p706jq4529p60znvw02hdf172qxgbdv59q0n7anqbr54";
      };
    });
    icu = pkgs.icu.overrideDerivation (old:
    let
      keywordFix = pkgs.fetchurl {
        url = "http://bugs.icu-project.org/trac/changeset/39484?format=diff";
        name = "icu-changeset-39484.diff";
        sha256 = "0hxhpgydalyxacaaxlmaddc1sjwh65rsnpmg0j414mnblq74vmm8";
      };
      pname = "icu4c";
    in
    rec {
      version = "58.2";
      name = "${pname}-${version}";
      src = pkgs.fetchurl {
        url = "http://download.icu-project.org/files/${pname}/${version}/${pname}-"
          + (stdenv.lib.replaceChars ["."] ["_"] version) + "-src.tgz";
        sha256 = "036shcb3f8bm1lynhlsb4kpjm9s9c2vdiir01vg216rs2l8482ib";
      };
      patches = [];
      postPatch = ''
        popd
        patch -p4 < ${keywordFix}
      '';
    });
  };
}
