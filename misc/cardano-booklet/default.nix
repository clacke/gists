{ pkgs ? import <nixpkgs> { }
, context ? pkgs.texlive.combined.scheme-context
, ruby ? pkgs.ruby
, infile ? pkgs.fetchurl {
    url = "https://cardanodocs.com/files/formal-specification-of-the-cardano-wallet.pdf";
    sha256 = "0b3scdrjfj7p7rqb5ywqwm4nsc8fyq5zj0yh25c32n1bl8d5vprz";
  }
, basename ? "formal-specification-of-the-cardano-wallet-a5a4"
}:

let infile1x1 =
pkgs.runCommand "${basename}.pdf" {
  buildInputs = [ context ruby ];
  inherit infile basename;
} ''
  texexec --pdfcombine --combination='1*1' --result=$basename $infile
  mv $basename.pdf $out
''; in

pkgs.runCommand "${basename}.pdf" {
  buildInputs = [ context ruby ];
  inherit infile1x1 basename;
} ''
  texexec --pdfarrange --printformat=up --paperformat=a5a4 --result=$basename $infile1x1
  mv $basename.pdf $out
''
