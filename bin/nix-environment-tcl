#!/usr/bin/env nix-shell
#! nix-shell -i tclsh -p tcl

# Execute things in an ephemeral nix environment context, with similar
# syntax as `guix environment --ad-hoc`:
# nix-environment <package_1> [ ... <package_n> ] -- \
#   command [ <arg_1> [ ... <arg_n>]]

lappend auto_path [exec nix-build --no-out-link --quiet -E {with import <nixpkgs> {}; tclx}]/lib
package require Tclx

set divider [lsearch -exact $argv --]
if { $divider == -1 } { set divider [llength $argv] }

set packages [lrange $argv -1 [expr $divider - 1]]
set cmd [lrange $argv [expr $divider + 1] [expr $divider + 1]]
set cmdArgs [list [lrange $argv [expr $divider + 2] [llength $argv]]]
set runner [exec nix-build --quiet --no-out-link --argstr cmd $cmd --argstr cmdArgs $cmdArgs --argstr autoPath [list $auto_path] -E {
  with import <nixpkgs> {};
  {autoPath, cmd, cmdArgs}:
  writeScript "runner.tcl" ''
    #!${tcl}/bin/tclsh
    set auto_path ${autoPath}
    package require Tclx
    execl ${cmd} ${cmdArgs}
  ''
}]

execl nix-shell [list -p {*}$packages --run $runner]
