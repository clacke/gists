function langless() {
  /var/guix/profiles/execline/bin/execlineb -c '
    importas -i language 1
    shift
    elgetpositionals
    pipeline -d {
      piperw 3 4
      background {
        fdmove 0 3
        fdmove 1 2
        grep -v "output left in -"
      }
      fdclose 3
      fdmove 2 4
      guix environment --ad-hoc enscript --
        enscript -E$language -wansi -o- $@
    }
    less -R
  ' "$@"
}

alias pyless='langless python'
alias bashless='langless bash'
alias javaless='langless java'
alias cg='ccd ~/git'

function ec() {
  emacsclient --alternate-editor $(type -p emacs) -n "$@" &>/dev/null &
}

function jsonless() {
  python -m json.tool "$1" | langless javascript -
}

function sudomy {
  cmd="$(type -p "$1")"
  shift
  sudo "$cmd" "$@"
}

function dockmy {
  instance="$1"
  cmd="$(readlink -e $(type -p "$2"))"
  shift; shift
  docker exec -it "$instance" /usr/bin/env TERM="$TERM" "$cmd" "$@"
}

function ccd {
  local dir="$PWD"
  local newdir

  while (( $# > 0 )); do
    newdir="$(cd "$dir"; readlink -e "$1")"
    if [[ -z $newdir ]]; then
      echo >&2 "'$1' cannot be found from '$dir'"
      return 2
    fi
    if [[ ! -d $newdir ]]; then
      echo >&2 "'$newdir' is not a directory"
      return 2
    fi
    dir=$newdir
    shift
  done
  cd "$dir"
}