[merge]
        tool = meld
[alias]
        lg = log --graph --pretty=tformat:'%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%an %ad)%Creset'
        lr = log --walk-reflogs --pretty=tformat:'%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%an %ad)%Creset'
        st = status --short
        amend = commit --amend --no-edit --reset-author
    	fixup = commit --fixup HEAD
        gco = !bash -c 'git fetch gerrit \"$1\" && git checkout FETCH_HEAD' git-gco
        fc = "!bash -ec 'change=\"$1\"; prefix=\"${change:$((${#change} - 2)):2}\"; git fetch gerrit \"refs/changes/${prefix}/${change}/*:refs/remotes/gerrit/changes/${change}/*\"' git-fc"
        cc = "!bash -ec 'change=\"$1\"; prefix=\"${change:$((${#change} - 2)):2}\"; git fc \"$change\"; latest=$(git ls-remote . \"refs/remotes/gerrit/changes/${change}/*\" | sort -n -t / -k 6 | cut -f 2 | tail -n 1); if [[ -z $latest ]]; then echo >&2 \"No such change: $change\"; exit 1; else git checkout \"$latest\"; fi' git-cc"
        cpc- = "!bash -ec 'change=\"$1\"; prefix=\"${change:$((${#change} - 2)):2}\"; latest=$(git ls-remote . \"refs/remotes/gerrit/changes/${change}/*\" | sort -n -t / -k 6 | cut -f 2 | tail -n 1); if [[ -z $latest ]]; then echo >&2 \"No such change: $change\"; exit 1; else git cherry-pick --allow-empty \"$latest\"; fi' git-cpc-"
        cpc = !git fc \"$1\" && git cpc- \"$1\"

        wta = worktree add --detach
        wtas = "!bash -ec 'if (( $# != 1)); then echo >&2 git wtas: 1 parameter expected; exit 2; fi; tree=\"$(python -c \"from __future__ import print_function; import os, os.path, sys; print(os.path.normpath(os.path.join(os.getenv(\\\"PWD\\\"), sys.argv[1])))\" \"$1\")\"; git wta \"$tree\"; cd \"$(git rev-parse --git-dir)\"; for mod in $(git config --blob HEAD:.gitmodules -l --name-only|gawk -F . \"/\\.path$/ {print \\$2}\"); do [ -d modules/$mod ] && git -C modules/$mod wta \"${tree}/$(git config --blob HEAD:.gitmodules --get submodule.${mod}.path)\"; done' wtas"

        # This is useful if you're on a host still on git 2.5 or something, which cannot fetch by hash on git submodule update.
        submodules-fetch = "!git ls-tree -r HEAD | awk \"\\$2 ~ /commit/\" | while read _ _ submod_hash submod_path; do  echo $submod_hash $submod_path; current_hash=$(env -u GIT_DIR git -C $submod_path rev-parse HEAD); if [[ $current_hash != $submod_hash ]]; then echo $current_hash; env -u GIT_DIR git -C $submod_path fetch origin $submod_hash; fi; done"
[core]
        autocrlf = false
        safecrlf = true
[uploadpack]
        allowTipSHA1InWant = true
        allowReachableSHA1InWant = true
[receive]
        denycurrentbranch = refuse